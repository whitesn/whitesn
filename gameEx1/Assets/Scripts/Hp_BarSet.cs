﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hp_BarSet : MonoBehaviour 
{
	public GameObject[] HpBarSet_OB;	//HP바 모음
	public void HpBar(GameObject tempOB)
	{
		bool On = false;	//현재 사용중인 Hp바인지 체크
		for(int aaa = 0; aaa < HpBarSet_OB.Length; aaa++)
		{
			if(HpBarSet_OB[aaa].activeSelf == true)
			{
				if(HpBarSet_OB[aaa].GetComponent<Hp_Bar>().tempOB == tempOB)
				{
					HpBarSet_OB[aaa].GetComponent<Hp_Bar>().timeTemp = 5f;
					On = true;
					break;
				}
			}
		}

		if(On == false)	//선택한 오브젝트에 HP바가 설정되지 않았을때
		{
			for(int aaa = 0; aaa < HpBarSet_OB.Length; aaa++)
			{
				if(HpBarSet_OB[aaa].activeSelf == false)
				{
					HpBarSet_OB[aaa].GetComponent<Hp_Bar>().tempOB = tempOB;
					HpBarSet_OB[aaa].GetComponent<Hp_Bar>().timeTemp = 5f;
					HpBarSet_OB[aaa].SetActive(true);
					break;
				}
			}
		}
	}
}
