﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {
	public float EnemyMin = -5f;
	public float EnemyMax = 5;
	public GameObject EnemySet_OB;
	// public GameObject[] EnemySet;

	//적 생성 코루틴
	IEnumerator Enemy_Create_Co()
	{
		do
		{
			// int temp = Random.Range(0,2);
			// GameObject Enemy_OB =  Instantiate(EnemySet[temp], 
			// 	new Vector2(Random.Range(EnemyMin,EnemyMax),
			// 	Random.Range(EnemyMin, EnemyMax)), gameObject.transform.rotation);

			int EnemyID = Random.Range(30001,30003);
			GameObject Enemy_OB =  Instantiate(Resources.Load("Monster/" + EnemyID) as GameObject, 
				new Vector2(Random.Range(EnemyMin,EnemyMax),
				Random.Range(EnemyMin, EnemyMax)), gameObject.transform.rotation);

			Enemy_OB.GetComponent<EnemyMove>().EnemyID = EnemyID;
			Enemy_OB.GetComponent<EnemyMove>().Enemy_Setting();
			Enemy_OB.GetComponent<Transform>().SetParent(EnemySet_OB.GetComponent<Transform>());	//하위 폴더로 넣기

			yield return new WaitForSeconds(3f);
		} while(true);
	}
	private void Start()
	{
		StartCoroutine(Enemy_Create_Co());
	}

	//HPbar 처리
	public GameObject Hpbar_OB;
	public void Hpbar(GameObject temp)
	{
		Hpbar_OB.GetComponent<Hp_BarSet>().HpBar(temp);
	}

	public Text GoldText;
	private void Update()
	{
		GoldText.text = "골드 : " + DataManager.Instance.gold.ToString("n0")+ "원";
	}

	//플레이어 스텟창 띄우기
	public GameObject StatPanel;
	public void PlayerStatPanel_Btn()
	{
		if(StatPanel.activeSelf == false)
		{
			StatPanel.SetActive(true);
		}else
		{
			StatPanel.SetActive(false);
		}
	}
}
