﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBox : MonoBehaviour 
{
	public Text InfoText;	//정보테스트를 보여줄곳
	public Image HpBar;

	public GameObject Temp_OB;	//보여줄 정보가 들어있는 오브젝트
	public float OpenTime = 5f;

	public string Name;
	public int Level;
	public int Atk;
	public int Def;
	public int MaxHp;
	public int CurrentHp;

	private void Update()
	{
		if(Temp_OB != null)
		{
			if(Temp_OB.tag == "Player")
			{
				Name = Temp_OB.GetComponent<PlayerMove>().Name;
				Level = Temp_OB.GetComponent<PlayerMove>().level;
				Atk = Temp_OB.GetComponent<PlayerMove>().atk;
				Def = Temp_OB.GetComponent<PlayerMove>().def;
				MaxHp = Temp_OB.GetComponent<PlayerMove>().MaxHp;
				CurrentHp = Temp_OB.GetComponent<PlayerMove>().CurrentHp;
			}else if(Temp_OB.tag == "Enemy")
			{
				Name = Temp_OB.GetComponent<EnemyMove>().Name;
				Level = Temp_OB.GetComponent<EnemyMove>().Level;
				Atk = Temp_OB.GetComponent<EnemyMove>().Atk;
				Def = Temp_OB.GetComponent<EnemyMove>().Def;
				MaxHp = Temp_OB.GetComponent<EnemyMove>().MaxHp;
				CurrentHp = Temp_OB.GetComponent<EnemyMove>().CurrentHp;
			}
			InfoText.text = "이름 : " + Name;
			InfoText.text += "\n레벨 : " + Level.ToString();
			InfoText.text += "\n공격력 : " + Atk.ToString("n0");	//n0은 세 자리가 넘어가면 콤마를 찍어준다.
			InfoText.text += "\n방어력 : " + Def.ToString("n0");
			InfoText.text += "\nHP : " + CurrentHp.ToString("n0") + " / " + MaxHp.ToString("n0");

			HpBar.fillAmount = (float)CurrentHp / (float)MaxHp;	//게이지 표시
		}else
		{
			gameObject.SetActive(false);	//선택한 것이 없으면 창을 끈다.
		}

		//시간이 지나면 창을 끈다.
		OpenTime -= 1f * Time.deltaTime;
		if(OpenTime < 0)
		{
			gameObject.SetActive(false);
		}
	}

}
