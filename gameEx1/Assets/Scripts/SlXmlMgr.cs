
#define SO_KOREA

using System;
using System.Xml;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;


public class SlXmlMgr
{
    private static SlXmlMgr XML;
    public static SlXmlMgr Instance
    {
        get
        {
            if (XML == null)
                XML = new SlXmlMgr();
            return XML;
        }
    }

    SortedList<string, SortedList<int, XmlNode>> tables
        = new SortedList<string, SortedList<int, XmlNode>>();

    SortedList<string, List<XmlNode>> tablesList = new SortedList<string, List<XmlNode>>();

	public string _language = "KOREA";
    string _langShort = "K";

    public SlXmlMgr()
    {
        XML = this;

        int lang = PlayerPrefs.GetInt("ENG_LANG");
        switch (lang)
        {
            case 0:
                _language = "ENGLISH";
                _langShort = "E";
                break;
            default:
                _language = "KOREA";
                _langShort = "K";
                break;
        }
    }

    ~SlXmlMgr()
    {
    }

	public void ChangeLang(int lang)
	{
		switch (lang)
		{
		case 0:
			_language = "ENGLISH";
			_langShort = "E";
			break;
		default:
			_language = "KOREA";
			_langShort = "K";
			break;
		}
		PlayerPrefs.SetInt("ENG_LANG", lang);
	}
    public bool HasTable(string name)
    {
        name = name.ToLower();
        if (tables.ContainsKey(name))
            return true;
        return false;
    }

    public bool AddTable(string name, string table)
    {
        name = name.ToLower();
        if (tables.ContainsKey(name))
            return false;

        XmlDocument doc = new XmlDocument();
        doc.LoadXml(table);

        XmlNode root = doc.SelectSingleNode("Root");
        XmlNodeList nodes = root.SelectNodes("Node");

        SortedList<int, XmlNode> nodeMap = new SortedList<int, XmlNode>(nodes.Count);
        List<XmlNode> nodeList = new List<XmlNode>();
        foreach (XmlNode node in nodes)
        {
            string nodeName = node.FirstChild.InnerText.Trim();
            try
            {
                nodeMap.Add(Convert.ToInt32(nodeName), node);
                nodeList.Add(node);
            }
            catch (System.Exception ex)
            {
                Debug.LogWarning(name + " : " + nodeName + " : " + " : " + ex.ToString());
            }
        }

        tables.Add(name, nodeMap);
        tablesList.Add(name, nodeList);
        return true;
    }

    public bool RemoveTable(string name)
    {
        name = name.ToLower();
        if (!tables.ContainsKey(name))
            return false;

        tablesList.Remove(name);
        tables.Remove(name);
        return true;
    }

    public int GetCount(string table)
    {
        table = table.ToLower();
        if (!tables.ContainsKey(table))
            return 0;
        SortedList<int, XmlNode> nodes = tables[table];
        if (nodes == null)
            return 0;

        return nodes.Count;
    }

    public float GetFloatByIndex(string tableName, string nodeName, int index)
    {
        return Convert.ToSingle(GetStringByIndex(tableName, nodeName, index));
    }

    public float GetFloat(string tableName, string nodeName, int key)
    {
        return Convert.ToSingle(GetString(tableName, nodeName, key));
    }

    public int GetIntByIndex(string tableName, string nodeName, int index)
    {
        return Convert.ToInt32(GetStringByIndex(tableName, nodeName, index));
    }

    public int GetInt(string tableName, string nodeName, int key)
    {
        try
        {
            return Convert.ToInt32(GetString(tableName, nodeName, key));
        }
        catch (System.Exception ex)
        {
        	Debug.LogWarning(tableName + " : " + nodeName + " : " + key + "\n" + ex);            
        }

        return 0;
    }

    public long GetLongByIndex(string tableName, string nodeName, int index)
    {
        return Convert.ToInt64(GetStringByIndex(tableName, nodeName, index));
    }


    public long GetLong(string tableName, string nodeName, int key)
    {
        return Convert.ToInt64(GetString(tableName, nodeName, key));
    }

    //@ Add. [2011.12.29 jin0e]
    public bool GetBoolByIndex(string tableName, string nodeName, int index)
    {
        int value = Convert.ToInt32(GetStringByIndex(tableName, nodeName, index));
        return (value == 0 ? false : true);
    }


    public bool GetBool(string tableName, string nodeName, int key)
    {
        int value = Convert.ToInt32(GetString(tableName, nodeName, key));
        return (value == 0 ? false : true);
    }

    public string GetStringByIndex(string tableName, string nodeName, int index)
    {
        tableName = tableName.ToLower();
        List<XmlNode> nodes = tablesList[tableName];
        if (nodes == null || nodes.Count <= index)
            return null;

        XmlNode data = nodes[index];

        if (data == null)
            return null;

        XmlNode result = data.SelectSingleNode(nodeName);
        if (result == null)
            return null;
        return result.InnerText.Trim();
    }
    public string GetStringByName(string tableName, string nodeName, string name)
    {
        tableName = tableName.ToLower();
        if (!tables.ContainsKey(tableName))
            return null;

        List<XmlNode> nodes = tablesList[tableName];

        foreach (XmlNode node in nodes)
        {
            XmlNode data = node.SelectSingleNode("Name");
            if (data.InnerText.Trim() == name)
            {
                XmlNode result = node.SelectSingleNode(nodeName);
                if (result == null)
                    return null;
                return result.InnerText.Trim();
            }
        }
        return null;
    }
    public string GetString(string tableName, string nodeName, int key)
    {
        tableName = tableName.ToLower();
        if (!tables.ContainsKey(tableName))
            return null;

        SortedList<int, XmlNode> nodes = tables[tableName];
        if (nodes == null)
            return null;

        if (nodes.ContainsKey(key))
        {
            XmlNode data = nodes[key].SelectSingleNode(nodeName);            
            if (data != null)
                return data.InnerText.Trim();
        }

        return null;
    }

    public string GetString(XmlNode node, string nodeName)
    {
        XmlNode data = node.SelectSingleNode(nodeName);
        if (data == null)
            return null;

        return data.InnerText.Trim();
    }

    public string GetText(string tableName, string nodeName, int key)
    {
        return GetString(tableName, nodeName+_langShort, key);
    }

    public string GetUIText(int key)
    {
        return GetString("string", _language, key);
    }

    public string GetErrorText(int key)
    {
        return GetString("errorCode_list", _language, key);
    }

    //         public string GetText(int key)
    //         {
    //             #if SO_KOREA
    //             return GetString("string", "KOREA", key.ToString());
    // #else
    //             return GetString("string", "English", key.ToString());
    // #endif
    //         }
    public string GetNameByIndex(string tableName, int index)
    {
        if (_language == "ENGLISH")
            return GetStringByIndex(tableName, "NameE", index);

        return GetStringByIndex(tableName, "NameK", index);
    }

    public string GetName(string tableName, int key)
    {
        if (_language == "ENGLISH")
            return GetString(tableName, "NameE", key);

        return GetString(tableName, "NameK", key);
    }

    public string GetExplane(string tableName, int key)
    {
        if (_language == "ENGLISH")
            return GetString(tableName, "ExplaneE", key);

        return GetString(tableName, "ExplaneK", key);
    }
    public string GetDesc(string tableName, int key)
    {
        if (_language == "ENGLISH")
            return GetString(tableName, "DescE", key);

        return GetString(tableName, "DescK", key);
    }


    public bool ContainKey(string tableName, int key)
    {
        tableName = tableName.ToLower();
        if (!tables.ContainsKey(tableName))
            return false;
        SortedList<int, XmlNode> nodes = tables[tableName];
        if (nodes == null)
            return false;

        if (nodes.ContainsKey(key))
        {
            return true;
        }

        return false;
    }
}



