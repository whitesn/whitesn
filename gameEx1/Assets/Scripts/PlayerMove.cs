﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMove : MonoBehaviour {

	[Header("플레이어 정보")]
	public string Name = "플레이어";
	public int level = 1;
	public int atk;
	public int def;
	public int MaxHp;
	public int MaxExp;
	public int CurrentHp;
	public int CurrentExp;

	//계산용변수
	int PlayerAtk;
	int PlayerDef;
	int PlayerMaxHp;

	public void PlayerSetting()
	{
		//기본 레벨에 따른 능력치 넣기
		PlayerAtk = SlXmlMgr.Instance.GetInt("PlayerData", "Atk", level);
		PlayerDef = SlXmlMgr.Instance.GetInt("PlayerData", "Def", level);
		PlayerMaxHp = SlXmlMgr.Instance.GetInt("PlayerData", "MaxHp", level);

		//스텟에 의한 계산
		PlayerAtk += (DataManager.Instance.Str * 3) + (DataManager.Instance.Dex * 1);
		PlayerDef += (DataManager.Instance.Con * 3) + (DataManager.Instance.Dex * 1);
		PlayerMaxHp += (DataManager.Instance.Con * 5) + (DataManager.Instance.Dex * 2);

		//스킬에 의한 계산

		//최종 수치 계산
		atk = PlayerAtk;
		def = PlayerDef;
		MaxHp = PlayerMaxHp;
		MaxExp = SlXmlMgr.Instance.GetInt("PlayerData", "MaxExp", level);
	}

	public Vector2 clickPoint;
	public GameObject click_OB;
	public Ray2D ray;
	public RaycastHit2D hit;
	public Vector2 dir;
	public bool isMoveState = false;
	public float moveSpeed = 3f;

	public Animator _ani;

	// public GameObject Enemy;

	public void PlayerAni_Idle()	//서있기
	{
		_ani.SetInteger("State",0);
	}
	public void PlayerAni_Run()	//걷기
	{
		_ani.SetInteger("State",1);
	}
	public void PlayerAni_Attack()	//공격하기
	{
		_ani.SetInteger("State",2);
	}
	public void PlayerAni_Die()	//죽었다.
	{
		_ani.SetInteger("State",3);
	}

	public GameObject InfoBoxPanel;

	public void InfoBoxOpen(GameObject temp)	//플레이어 정보창
	{
		InfoBoxPanel.GetComponent<InfoBox>().Temp_OB = temp;	//temp를 InfoBox 스크립트에 Temp_OB에 넣어주어라
		InfoBoxPanel.SetActive(true);
		InfoBoxPanel.GetComponent<InfoBox>().OpenTime = 5f;
	}
	private void Update()
	{
		if(EventSystem.current.IsPointerOverGameObject()==false)	//UI를 클릭한게 아니라면 실행
		{
		if(Input.GetMouseButtonDown(0))	//마우스 클릭
		{
			clickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			ray = new Ray2D(clickPoint,Vector2.zero);
			hit = Physics2D.Raycast(ray.origin, ray.direction);	//터치 시 레이저를 쏴주는 듯이 해서 좌표를 찍음
			click_OB = hit.transform.gameObject;
			isMoveState = true;

			if(hit.transform.gameObject.tag == "Player")
			{
				InfoBoxOpen(hit.transform.gameObject);
			}else if(hit.transform.gameObject.tag == "Enemy")
			{
				InfoBoxOpen(hit.transform.gameObject);
			}
		}
		}
		if(isMoveState == true)	//이동
		{
			dir = clickPoint - new Vector2(transform.position.x, transform.position.y);
			transform.Translate(dir.normalized * moveSpeed * Time.deltaTime);
			PlayerAni_Run();

			if(dir.x < 0 )
			{
				_ani.transform.rotation = Quaternion.Euler(new Vector3(_ani.transform.position.x, 180, 0));
			}else{
				_ani.transform.rotation = Quaternion.Euler(new Vector3(_ani.transform.position.x, 0, 0));
			}
		}

		float distance = Vector2.Distance(gameObject.transform.position, clickPoint);
		if(distance < 0.2f)	//멈춤
		{
			isMoveState = false;
			PlayerAni_Idle();
		}

		//적 체크 공격.
		if(click_OB != null)
		{
			if(click_OB.tag == "Enemy")
			{
				float Enemydistance = Vector2.Distance(gameObject.transform.position, click_OB.transform.position);
				// float Enemydistance = Vector2.Distance(gameObject.transform.position, Enemy.transform.position);
				if(Enemydistance < 1f)	//적이 사정거리안에 들어오면
				{
					isMoveState = false;
					PlayerAni_Attack();
				}else
				{
					clickPoint = click_OB.transform.position;
					isMoveState = true;
				}
			}
		}
	}


	public void AttackEnemy()
	{
		if(click_OB != null)
		{
			if(click_OB.tag == "Enemy")
			{
				float distance2 = Vector2.Distance(gameObject.transform.position, click_OB.transform.position);
				
				if(distance2 < 1f)
				{						
					Debug.Log("적 때렸다");
					click_OB.GetComponent<EnemyMove>().Damage(gameObject,atk);
				}
			}
		}
		else
			PlayerAni_Idle();
	}

	private void Start()
	{
		PlayerSetting();	//플레이어 능력치 셋팅
		CurrentHp = MaxHp;	//플레이어 체력 최고 회복
		clickPoint = transform.position;
	}

	//경험치 계산
	public void Ext_C(int expTemp, string type)
	{
		switch(type) 
		{
			case "+":
				CurrentExp += expTemp;
				break;
			case "-":
				CurrentExp -= expTemp;
				break;
			case "*":
				CurrentExp *= expTemp;
				break;
			case "/":
				CurrentExp /= expTemp;
				break;
		}
		//레벨업이 가능한지 비교해서 레벨업 진행
		if(MaxExp <= CurrentExp)
		{
			//레벨업
			level += 1;
			PlayerSetting();	//새로운 레벨에 의한 능력치 셋팅
			CurrentHp = MaxHp;	//현재 체력을 max로 회복
			DataManager.Instance.StatPoint += 2;	//스텟포인트

			//레벨업 효과
		}
		//저장
	}

	GameObject gameManager;
	private void Awake()
	{
		gameManager = GameObject.FindGameObjectWithTag("GameManager");	//GameManager라는 태그를 갖고 있는 얘를 찾아서 넣어라
	}
	//맞았을때 처리
	public void Damage(int atk, GameObject Enemy)
	{
		//데미지 처리
		int atkDamage = atk;
		atkDamage = atkDamage - def;	//방어력 체크
		
		if(atkDamage <= 0)
		{
			//방어성공. 데미지 없음
		}else
		{
			CurrentHp -= atkDamage;
			//색상변화
			_ani.transform.GetComponent<SpriteRenderer>().color = new Color32(255,0,0,255);
			Invoke("ColorReset",0.2f);
		}
		//적의 체력 정보를 게임매니저로 던저주기
		gameManager.GetComponent<GameManager>().Hpbar(gameObject);

		// //넉백
		// Vector2 dir = player.transform.position - transform.position;
		// transform.position -= new Vector3(dir.x * 0.5f, dir.y * 0.5f,0);

		//죽음 확인
		if(CurrentHp <= 0)	//죽었는지 확인
		{
			//죽었네
			Destroy(gameObject);
		}
	}
	void ColorReset()
	{
		_ani.transform.GetComponent<SpriteRenderer>().color = new Color32(255,255,255,255);
	}
}
