﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hp_Bar : MonoBehaviour 
{
	public Image HpBar_Image;
	public float MaxHp;
	public float CurrentHp;
	public GameObject tempOB;
	public float timeTemp = 5f;

	private void Update()
	{
		if(tempOB != null)
		{
			if(tempOB.tag == "Player")
			{
				MaxHp = tempOB.GetComponent<PlayerMove>().MaxHp;
				CurrentHp = tempOB.GetComponent<PlayerMove>().CurrentHp;
			}
			else
			{
				MaxHp = tempOB.GetComponent<EnemyMove>().MaxHp;
				CurrentHp = tempOB.GetComponent<EnemyMove>().CurrentHp;
			}
		
			HpBar_Image.fillAmount = CurrentHp / MaxHp;
			if(CurrentHp <= 0)
			{
				Close();
			}
		}else
		{
			Close();
		}

		timeTemp -= 1f * Time.deltaTime;
		if(timeTemp < 0)
		{
			Close();
		}
	}

	void Close()
	{
		tempOB = null;
		gameObject.SetActive(false);
	}
	private void LateUpdate()
	{
		if(tempOB != null)
		{
			gameObject.transform.position = Camera.main.WorldToScreenPoint(tempOB.transform.position + new Vector3(0,1f,0));
		}
	}

}
