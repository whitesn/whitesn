﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

	public int EnemyID;	//캐리터ID

	public string Name;
	public int Level = 1;
	public int Atk;
	public int Def;
	public int MaxHp;
	public int Exp;	//플레이어에게 줄 경험치
	public int Gold;	//적이 들고 있는 재화

	public int CurrentHp;

	GameObject gameManager;
	private void Awake()
	{
		gameManager = GameObject.FindGameObjectWithTag("GameManager");	//GameManager라는 태그를 갖고 있는 얘를 찾아서 넣어라
		TargetSetting();
	}
	//캐릭터 셋팅
	public void Enemy_Setting()
	{
		Name = SlXmlMgr.Instance.GetString("EnemyData", "Name", EnemyID);
		Atk = SlXmlMgr.Instance.GetInt("EnemyData", "Atk", EnemyID);
		Def = SlXmlMgr.Instance.GetInt("EnemyData", "Def", EnemyID);
		MaxHp = SlXmlMgr.Instance.GetInt("EnemyData", "MaxHp", EnemyID);
		Exp = SlXmlMgr.Instance.GetInt("EnemyData", "Exp", EnemyID);
		Gold = SlXmlMgr.Instance.GetInt("EnemyData", "Gold", EnemyID);
		CurrentHp = MaxHp;
	}

	//맞았을때 처리
	public void Damage(GameObject player, int atk)
	{
		//데미지 처리
		int atkDamage = atk;
		atkDamage = atkDamage - Def;	//방어력 체크
		
		if(atkDamage <= 0)
		{
			//방어성공. 데미지 없음
		}else
		{
			CurrentHp -= atkDamage;
			//색상변화
			_ani.transform.GetComponent<SpriteRenderer>().color = new Color32(255,0,0,255);
			Invoke("ColorReset",0.2f);
		}
		//적의 체력 정보를 게임매니저로 던저주기
		gameManager.GetComponent<GameManager>().Hpbar(gameObject);

		//넉백
		Vector2 dir = player.transform.position - transform.position;
		transform.position -= new Vector3(dir.x * 0.5f, dir.y * 0.5f,0);

		//죽음 확인
		if(CurrentHp <= 0)
		{
			//돈을 떨어트림, 경험치 준다. 아이템(확률적으로)준다
			player.GetComponent<PlayerMove>().Ext_C(Exp, "+");	//경험치
			DataManager.Instance.Gold_C(Gold, "+"); 	//골드 획득
			//죽었네
			//죽음 모션
			Destroy(gameObject);
		}
	}

	public Animator _ani;	//애니메이터

	//idel
	public void EnemyAni_Idle()
	{
		_ani.SetInteger("State",0);
	}
	//walk
	public void EnemyAni_Walk()
	{
		_ani.SetInteger("State",1);
	}
	//idel
	public void EnemyAni_Attack()
	{
		_ani.SetInteger("State",2);
	}
	//idel
	public void EnemyAni_Die()
	{
		_ani.SetInteger("State",3);
	}
	
	public GameObject moveTarget;	//이동타겟
	bool isMoveState = false;	//이동
	public Vector2 Target;
	public bool EnemyDie = false;
	public float moveSpeed = 1f;
	public float AtkDistance = 1f; //사정거리

	GameObject player;

	public GameObject Sight_OB;	//시야

	//움직임 타겟잡기
	public void TargetSetting()
	{
		moveTarget.transform.position = new Vector3(gameObject.transform.position.x + 
			Random.Range(-5f, 5f), gameObject.transform.position.y + Random.Range(-5f, 5f), 0);
		Target = moveTarget.transform.position;
		isMoveState = true;

		////1. (월드 좌표계에서 지역 좌표계로 이동시키기)

		// Vector2 dir = TargetJoint2D.position - gameObject.transform.position;
	}

	private void Update()
	{
		if(EnemyDie == false)
		{
			//이동
			if(isMoveState == true)
			{
				Vector2 dir = Target - new Vector2(transform.position.x, transform.position.y);
				transform.Translate(dir.normalized * moveSpeed * Time.deltaTime);
				EnemyAni_Walk();

				if(dir.x < 0)
				{
					_ani.transform.rotation = Quaternion.Euler(new Vector3(_ani.transform.position.x,180,0));
					Sight_OB.transform.rotation = Quaternion.Euler(new Vector3(_ani.transform.position.x,0,180));
				}
				else
				{

					_ani.transform.rotation = Quaternion.Euler(new Vector3(_ani.transform.position.x, 0, 0));
					// Sight_OB.transform.rotation = Quaternion.Euler(new Vector3(0,0,180));
					Sight_OB.transform.rotation = Quaternion.Euler(new Vector3(_ani.transform.position.x,0,0));
				}
			}

			//플레이어가 시야에 들어오면 타겟을 플레이로 변경
			Sight_OB.GetComponent<Sight2D>().FindViewTargets();
			if(Sight_OB.GetComponent<Sight2D>().hitedTargetContainer.Count > 0)
			{
				player = GameObject.FindGameObjectWithTag("Player");
				Target = player.transform.position;

								//플레이어를 찾아서 근처에 있으면 공격
				player = GameObject.FindGameObjectWithTag("Player");
				float distance2 = Vector2.Distance(gameObject.transform.position, player.transform.position);
				if(distance2 < AtkDistance)	//플레이어가 ㅏ정거리안에 있음
				{
					EnemyAni_Attack();
					isMoveState = false;
				}
				else	//사정거리 밖
				{
					isMoveState = true;
				}
			}

			//클릭 체크해서 액션
			float distance = Vector2.Distance(gameObject.transform.position, Target);
			//클릭지점 근처에 오면 멈춤
			if(distance < 0.02f)
			{
				EnemyAni_Idle();
				isMoveState = false;
				TargetSetting();
			}			
		}
	}
	//공격하기
	public void AttackPlayer()
	{
		if(EnemyDie == false)
		{
			float distance2 = Vector2.Distance(gameObject.transform.position, player.transform.position);
			if(distance2 < AtkDistance)	//플레이어가 사정거리안에 있음
			{
				 
				player.GetComponent<PlayerMove>().Damage(Atk, gameObject);
			}else	//사정거리 밖
			{
				EnemyAni_Idle();
			}
		}
	}

	void ColorReset()
	{
		_ani.transform.GetComponent<SpriteRenderer>().color = new Color32(255,255,255,255);
	}
}
