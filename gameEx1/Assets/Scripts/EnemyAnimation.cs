﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour {
	void Attack()
	{
		gameObject.GetComponentInParent<EnemyMove>().AttackPlayer();
	}
	void Idle()
	{
		gameObject.GetComponentInParent<EnemyMove>().EnemyAni_Idle();
	}
}
