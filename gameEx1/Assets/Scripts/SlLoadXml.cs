using UnityEngine;
using System;
using System.IO;
//using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
[ExecuteInEditMode]
public class SlLoadXml : MonoBehaviour {

    public bool _isLoadFromPatch = false;
    public string[] urlList;
    public TextAsset[] xmlList;

	void Awake ()
    {
        foreach (string url in urlList)
        {
            StartCoroutine(waitLoadXml(url));
        }
        if (xmlList != null)
        {
            foreach (TextAsset asset in xmlList)
            {
                if (asset != null)
                    ReadXml(asset);
            }
        }
	}

    public void ReadXml(TextAsset asset)
    {
        SlXmlMgr.Instance.AddTable(asset.name, asset.text);
    }
    WWW download;
    IEnumerator waitLoadXml(string url)
    {    
        string fullUrl = "assetbundles/"+url +".x";


        if (fullUrl.IndexOf("file://") == 0 || fullUrl.IndexOf("http://") == 0)
            download = new WWW(fullUrl);
        //if (Application.platform == RuntimePlatform.OSXWebPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
        //    download = new WWW(fullUrl);
        //else// if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
        //    download = new WWW("file://" + Application.dataPath + "/../" + fullUrl);

        yield return download;

        AssetBundle assetBundle = download.assetBundle;

        if (assetBundle != null)
        {
            TextAsset go = (TextAsset)assetBundle.mainAsset;

            if (go != null)
                SlXmlMgr.Instance.AddTable(url, go.text);
            else
                Debug.Log("Couldnt load resource");
        }
        else
        {
            Debug.Log("Couldnt load resource");
        }
    }
}
