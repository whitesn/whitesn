﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour {

	static DataManager instance;
	public static DataManager Instance
	{
		get
		{
			return instance;
		}
	}

	private void Awake()
	{
		if(instance == null)
		{
			DontDestroyOnLoad(gameObject);
			instance = this;
		}
		else
		{
			DestroyObject(gameObject);
		}
	}

	public int gold = 0;	//골드
	public int Str = 1;	//힘
	public int Con = 1;	//건강
	public int Dex = 1; 	//민첨
	public int StatPoint = 0;	//스테소인트

	//골드 획득 계산
	public void Gold_C(int GoldTemp, string type)
	{
		switch(type)
		{
			case "+":
				gold += GoldTemp;
				break;
			case "-":
				gold -= GoldTemp;
				break;
			case "*":
				gold *= GoldTemp;
				break;
			case "/":
				gold /= GoldTemp;
				break;
		}
	}
}
