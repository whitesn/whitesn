﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

	void Attack()
	{
		gameObject.GetComponentInParent<PlayerMove>().AttackEnemy();
	}
	void Idle()
	{
		gameObject.GetComponentInParent<PlayerMove>().PlayerAni_Idle();
	}
}
