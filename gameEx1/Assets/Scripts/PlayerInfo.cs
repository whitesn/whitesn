﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {

	public Text StrText;
	public Text ConText;
	public Text DexText;
	public Text StatPointTextt;

	private void Update()
	{
		StrText.text = "힘 : " + DataManager.Instance.Str.ToString();
		ConText.text = "건강 : " + DataManager.Instance.Con.ToString();
		DexText.text = "민첨 : " + DataManager.Instance.Dex.ToString();
		StatPointTextt.text = "스텟포인트 : " + DataManager.Instance.StatPoint.ToString();
	}

	//스텟을 올리기 버튼
	public void Stat_Btn(string temp)
	{
		if(DataManager.Instance.StatPoint > 0)
		{
			DataManager.Instance.StatPoint -= 1;
			switch(temp)
			{
				case "Str":
					DataManager.Instance.Str += 1;
					break;
				case "Con":
					DataManager.Instance.Con += 1;
					break;
				case "Dex":
					DataManager.Instance.Dex += 1;
					break;				
			}

			GameObject player = GameObject.FindGameObjectWithTag("Player");
			player.GetComponent<PlayerMove>().PlayerSetting();
		}
	}

}
